#include <iostream>
#include <fstream>

#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>

#include "insulinemodel.h"
#include "vectormath.h"
#include "ordetests.h"

using namespace std;


int main(int argc, char *argv[]){
    // define a set of parameters as 'reference' solution
    double p1 = 0.025, p2 = 0.01, Gb = 60., Si = 0.0005, I = 50.;
    //Gb =  60.; p1 =  0.00043; p2 =  0.0135; Si=  4.15e-005;
    Gb = 60.; p1 = 0.00012; p2 = 0.0135 ; Si = 4.17e-5;
    vector<double> R(33, 0.0);
    double dt = 1.0;
    state_type x0(2, 0);
    x0[0] = 300.;
    x0[1] = 0.;
    InsulineModel model(Gb, p1, p2, Si, I);
	
    // solve the system and store the reference solution
    vector<state_type> x;
    vector<double> time;
    ODEObserver sol(x, time);
    solveODE(x0, model, dt, sol);
    
    // and write the solution to a text file
    ofstream outfile("solutions/referencep1.out");
    ofstream outfilenoise("solutions/referencep1_noise.out");
    outfile.precision(16);
    boost::normal_distribution<> gauss(0.0, 5.);
    boost::normal_distribution<> gauss2(0.0, 0.1);
    boost::random::mt19937 gen;
    
    for( int i = 0 ; i < time.size() ; ++i ){
        outfile<<time[i]<<' '<<x[i][0]<<' '<<x[i][1]<<endl;
        
        // now add gaussian noise with a given constant variance per sample
        double n = gauss(gen);
        double e = gauss2(gen);
        outfilenoise<<time[i]<<' '<<x[i][0] + n<<' '<<x[i][1] + e<<endl;
    }
    
    return 0;
}