/********************************************************************************
 *
 * Templated library for performing the most basic vector<double> operations.
 *
 * Author: Hannes Vandecasteele
 *
 ********************************************************************************/
#ifndef __VECTORMATH__
#define __VECTORMATH__

#include <algorithm>
#include <iostream>

using namespace std;

/**
 * Add two compatible vector<double>s of the same length.
 */
vector<double> operator+(const vector<double>& x, const vector<double>& y ){
    assert( x.size() == y.size() );
   
    vector<double> res(x.size());
    for( size_t i = 0 ; i < x.size() ; ++i ){
        res[i] = x[i] + y[i];
    }
    
    return res;
}

/**
 * Subtract two compatible vector<double>s of the same length
 */
vector<double> operator-(const vector<double>& x, const vector<double>& y ){
    assert(x.size() == y.size() );
    
    vector<double> res(x.size());
    for( size_t i = 0 ; i < x.size() ; ++i ){
        res[i] = x[i] - y[i];
    }
    
    return res;
}

/**
 * Multiply a vector<double> with a constant factor
 */
vector<double> operator*(double a, const vector<double>& x ){
    vector<double> res(x.size());
    for( size_t i = 0 ; i < x.size() ; ++i ){
        res[i] = a*x[i];
    }
    
    return res;
}

/**
 * Divide a vector<double> by a constant number.
 */
vector<double> operator/(const vector<double>& x, double a){
    vector<double> res(x.size());
    for( size_t i = 0 ; i < x.size() ; ++i ){
        res[i] = x[i]/a;
    }
    
    return res;
}

/** 
 * Calculate the 2-norm of a vector<double>.
 */
double norm(const vector<double>& x){
    return sqrt(inner_product(x.begin(), x.end(), x.begin(), 0.0));
}

/**
 *  Retain the fractional part of a vector<double>.
 */
vector<double> mod1(const vector<double>& x ){
    vector<double> res(x.size());
    for( size_t i = 0 ; i < x.size() ; ++i ){
        res[i] = x[i] - (long long)x[i];
    }
    
    return res;
}

/**
 * Compute the mean of a vector<double>.
 */
double mean(const vector<double>& x) {
	double res = 0.;
	for (size_t i = 0 ; i < x.size() ; ++i ) {
        res += x[i];
	}
	return res/x.size();
}
 
/**
 * Compute the standard deviation of the average estimator (CLT).
 */
double stddev(const vector<double>& x) {
	double res = 0.;
    double m = mean(x);
	for (size_t i = 0 ; i < x.size() ; ++i ) {
        res += (x[i] - m)*(x[i] - m);
    }
    res /= ( (x.size()-1) * x.size() );
	return sqrt(res);
}
 
/**
 * Print a given vector.
 */
void print( vector<double> v ){
    for( double x: v ){
        cout<<x<<" ";
    }
    cout<<endl;
}

/** 
 * Compute the fractional part of a real number.
 */
double mod1(double d ){
    return d - (long long)d;
}


#endif