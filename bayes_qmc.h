/********************************************************************************
 *
 * Bayesian paramter estimation for the ODE insuline model.
 *
 * Author: Hannes Vandecasteele
 *
 *******************************************************************************/
#ifndef __BAYESQMC__
#define __BAYESQMC__

#include <boost/random/normal_distribution.hpp>
#include <boost/math/special_functions/erf.hpp>
#include <boost/math/distributions/lognormal.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <queue>

#include "insulinemodel.h"
#include "vectormath.h"
#include "estimators.h"
#include "sampler.h"

using namespace boost::random;
using namespace boost::math;
using namespace std;

/***************** 
 * Represent the solution.
 */
struct Solution {
    vector<state_type> x;
    vector<double> time;
    double mse;
};

/** 
 * Compare for the priority queue.
 */
class compare{
     
public:
    bool operator()( const Solution& s1, const Solution& s2 ){
       return s1.mse < s2.mse; 
    }
};
 
/**
 * Store the best solutions.
 */
priority_queue<Solution, std::vector<Solution>, compare > best_mse;
size_t max_size = 10;


/**
 * Structure to store the estimation of a parameter plus meta information.
 */
struct Estimation {
    // mean and var
    string variable;
    vector<double> estimates;
                                             
    // store the posterior distribution      
    vector<double> abscis;                   
    vector<double> density;                  
                                             
    // print a short summary of this estimation
    void print(){                            
        cout<<"Statistics for "<<variable<<endl;
        
        /*cout<<"posterior: "<<endl;
        for( size_t i = 0 ; i < abscis.size() ; ++i ){
            cout<<abscis[i]<<" "<<density[i]<<endl;
        }*/
    }
	
	// write the results to a file with format: mean stddev; number of estimates; estimates; abscis density
	void write(string filename){
        cout<<"Writing to "<<filename<<endl;
		ofstream outfile(filename);
		outfile.precision(16);
		
        outfile<<estimates.size()<<endl;
		for( size_t i = 0; i < estimates.size() ; ++i) {
			outfile<<estimates[i]<<endl;
		}
        
		for( size_t i = 0 ; i < abscis.size() ; ++i ){
            outfile<<abscis[i]<<' '<<density[i]<<endl;
        }
	}
};

/** 
 * Simulate the ODE Model given the parameters. Return the mse of the solution.
 */
double simulate_model( const vector<double>& params, const ODEObserver& ref ){
    // Create solver parameters
    InsulineModel model(params[0], params[1], params[2], params[3], 50.);
    state_type x0(2, 0.0); x0[0] = 300.; x0[1] = 0.0;
    vector<state_type> x;
    double dt = 1.0;
    vector<double> time;
    ODEObserver sol(x, time);
    
    // Solve the ODE system
    solveODE(x0, model, dt, sol);
    
    // Compute NLS error with reference signal and return this value.
    double sigma = 0.1;
    double mse = 0.0;
    for( size_t i = 0 ; i < time.size() ; ++i ){
        double error  = ref._x[i][0] - x[i][0];
        double error2 = ref._x[i][1] - x[i][1];
        //mse += (error*error + error2*error2) / sigma;
        mse += error*error/sigma;
    }
    mse /= time.size();
    
    // and return the mse
    return mse;
}

/**
 * Compute an estimate for a given function, by rescaling the mse's relative to 1.
 */
double compute_estimate(const vector<double>& mse, const vector<double>& fval){
    // find the minimal mse.
    double m = *(min_element(mse.begin(), mse.end()));
    
    // define the integrals
    double numerator = 0.0;
    double denom = 0.0;
    
    for( size_t i = 0 ; i < mse.size() ; ++i ){
        if( isnan(fval[i]) || isnan(mse[i]) ) continue;
        
        // compute the integral and the normalization constant, for each function
        numerator += fval[i] * exp(-(mse[i] - m));
        denom += exp(-(mse[i] - m));
    }
    
    // and return the estimate
    return numerator / denom;
}

/**
 * Perform the monte carlo simulation for a given function
 */
double monte_carlo_simulation(QMCFunction& function, vector<double>& mse, 
                              vector<vector<double> >& points, const vector<double>& z, 
                              size_t n, const ODEObserver& reference){
    // generate the shifts
    size_t s = z.size();
    vector<double> shift(s, 0.0);
    for( size_t j = 0 ; j < s ; ++j ) shift[j] = u(gen);
    
    // create class to sample the parameter prior distributions
    Sampler sampler;
    
    // store the function values
    vector<double> fvals(n, 0.0);
    points.resize(n);
    
    // Iterate over the lattice points
    for( size_t k = 0 ; k < n ; ++k){
        // sample the next lattice points
        vector<double> x(s, 0.0);
        for( size_t a=0 ; a<s ; ++a) x[a] = mod1(((double)k*z[a])/n + shift[a]);
        vector<double> params = sampler.sample(x);
        points[k] = params;
        
        // and compute the function value
        double y = simulate_model(params, reference);
        mse[k]   = y;
        fvals[k] = function(params);
    }
    
    // return the integral value
    return compute_estimate(mse, fvals);
}

/**
 * Make a posterior distribution of a given function.
 */
void compute_posterior( const vector<vector<double> >& points, const vector<double>& mse, Estimation& est ){
    // posterior variables
    double minval = 0.001; //1.e-5;
    double maxval = 1.e-4; //9.4e-5;
    cout<<"min "<<minval<<" "<<maxval<<endl;
    cout<<"sampling posterior"<<endl;
    int nbsamples = 1000;
    double q = 1.e-6;
    
    // find the minimal mse.
    double m = *(min_element(mse.begin(), mse.end()));
    
    for( int i = 0 ; i < nbsamples ; ++i ){
        // sample point for the posterior density
        double x = minval + i* (maxval-minval)/nbsamples;
        //
        //cout<<"sampling post at "<<x<<endl;
        
        // define the integrals
        double numerator = 0.0;
        double denom = 0.0;
        
        for( size_t i = 0 ; i < mse.size() ; ++i ){            
            // evaluate guassian distribution
            double fval = points[i][3];
            double val = 1./(q*sqrt(2.*atan(1.)*4.))*exp(-(x-fval)*(x-fval)/(2.*q*q));
            if( isnan(val) || isnan(mse[i]) ) continue;
            
            // compute the integral and the normalization constant, for each function
            numerator += val * exp(-(mse[i] - m));
            denom += exp(-(mse[i] - m));
        }
        
        // and store the value
        est.abscis.push_back(x);
        est.density.push_back(numerator/denom);
    }  
}    

    
/**
 * Estimate a given function using QMC integration and Bayesian inference.
 */
Estimation estimate_qmc(QMCFunction& estimators, vector<double> z, size_t n, int shifts, const ODEObserver& reference){ 
    // setup the return variable
    Estimation result;
    result.variable = estimators.getName();
    
    // Run the QMC solver 16 times to estimate the error(variance)
    for( size_t i = 0 ; i < shifts ; ++i ){
        //best_mse.clear();
        cout<<"computing "<<i<<"-th estimate"<<endl;
        
        // store all function evaluations and all mse's in a vector
        vector<double> mse(n, 0.0);
        vector<vector<double> > points(n);
        
        // and do the monte carlo simulation
        double est = monte_carlo_simulation(estimators, mse, points, z, n, reference);
       
        // and now compute the estimations and meta information
        cout<<"estimate "<<est<<endl;
        result.estimates.push_back(est);

        // and compute the posterior distribution for the first estimate.
        if( i == 0 ){
            compute_posterior(points, mse, result);
            //result.print();
        }
    }
    
    // And return the data        
    return result;
}

#endif