#include <fstream>
#include <string>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "bayes_qmc.h"

using namespace std;

/**
 * Retreive a generating vector from file.
 */
vector<double> read_vector(string fname, int s = 250){
    ifstream infile(fname);
    vector<double> v;
    string input;
    getline(infile, input);
    int k = 1;
    
    while( k <= s && input != "" ){
        v.push_back(atoi(input.c_str()));
        getline(infile, input);
    }
    
    return v;
}


/**
 * Read a reference solution from a file.
 */
void read_file(string filename, vector<double>& time, vector<state_type>& x){
    ifstream infile(filename);
    string input;
    getline(infile, input);
    
    while( input != "" ){
        // split the string
        vector<string> tokens;
        boost::split(tokens, input, boost::is_any_of(" \t"));
        
        // time is the first component
        time.push_back(boost::lexical_cast<double>(tokens[0]));
        
        //and read the state type
        state_type state;
        for( size_t i = 1 ; i < tokens.size() ; ++i ){
            state.push_back(boost::lexical_cast<double>(tokens[i]));
        }
        x.push_back(state);
        
        // and read the next line
        getline(infile, input);
    }
}

/**
 * Compute confidence intervals of a given estimations.
 *
 * TODO: relocate this function to a more suitable place.
 */
void compute_CI(Estimation &est, double p, double& cimin, double& cimax ){
    // find cimin
    double mass = 0.0;
    for( size_t i = 1 ; i < est.abscis.size() ; ++i ){
        mass += 0.5*(est.abscis[i] - est.abscis[i-1])*(est.density[i] + est.density[i-1] );
        
        if( mass > p/2. ){
            cimin = est.abscis[i];
            break;
        }
    }
    
    // find cimax
    mass = 0.0;
    for( size_t i = est.abscis.size()-1 ; i > 0 ; --i ){
        mass += 0.5*(est.abscis[i] - est.abscis[i-1])*(est.density[i] + est.density[i-1] );
        
        if( mass > p/2. ){
            cimax = est.abscis[i-1];
            break;
        }
    }
}

int main(int argc, char *argv[]){
    // read the reference solution from file
    vector<double> time;
    vector<state_type> x;
    read_file("solutions/reference_noise.out", time, x);
    ODEObserver ref(x, time);
    
    // read the generating vector
    vector<double> z = read_vector("exod2_base2_m20_CKN.txt", 4);

    // set the functions to estimate
    QMCFunction *si;
    bool posterior = false, order = false;
    int shifts = 16;
    int points = 1<<18;
    for( int i = 1 ; i < argc ; ++i ){
        if( strcmp(argv[i], "-estimator") == 0 ){
            string es = argv[i+1];
            if( es == "si" ){
                si = new SiEstimator;
            } else if( es == "gb" ){
                si = new GbEstimator;
            } else if( es == "p1" ){
                si = new P1Estimator;
            } else if( es == "p2" ){
                si = new P2Estimator;
            } else {
                cerr<<"Pass a function to estimate"<<endl;
                return 0;
            }
            i++;
        } else if( strcmp(argv[i], "-posterior") == 0 ){
            posterior = true;
        } else if( strcmp(argv[i], "-order") == 0 ){
            order = true;
        } else if( strcmp(argv[i], "-s") == 0 ){
            shifts = atoi(argv[i+1]);
            i++;
        } else if ( strcmp(argv[i], "-n") == 0 ){
            points = ( 1 << atoi(argv[i+1]));
            i++;
        }
    }
    
    // define the file for the order plot
    ofstream orderfile("solutions/order.out");
    
    if( order ){
        // plot convergence plot
        for( int n = 1 ; n <= 20 ; ++n ){
            // estimate the functions
            cout<<endl<<endl<<"Estimation for "<<(1<<n)<<" points"<<endl;
            Estimation est = estimate_qmc(*si, z, (1<<n), shifts, ref);
        
            cout<<"mean "<<mean(est.estimates)<<endl;
            cout<<"stddev "<<stddev(est.estimates)<<endl;
            
            // and write the results
            orderfile<<n<<" "<<stddev(est.estimates)<<endl;

        }
    } else {
        // estimate the functions
        Estimation est = estimate_qmc(*si, z, points, shifts, ref);
    
        cout<<"mean "<<mean(est.estimates)<<endl;
        cout<<"stddev "<<stddev(est.estimates)<<endl;
        est.write("solutions/posterior_p1.out");
    }
    
    
    return 0;
}