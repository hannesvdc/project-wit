function [mu, sigma] = lognormal(m, v )
    mu = log(sqrt(m/sqrt(1+v/(m*m))));
    sigma = sqrt(log(1+v/(m*m)));
end