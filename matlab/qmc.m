% Monte Carlo trial example
z = z(1:2);
f = @(x) prod(1.0 + x.*x - x + 1./6);
errors = [];
ns = [];

for n = 10:18
    integrals = zeros(1, 8);
    N = 2^n;
    
    for k = 1:8
        shift = rand(2,1);
        x = mod1(z*(0:N-1)/N + shift);
        feval = f(x);

        integrals(k) = mean(feval);
    end

    avg = mean(integrals);
    error = sqrt(var(integrals));
    ns = [ns, n];
    errors = [errors, error];
end

semilogy(ns, errors);
hold on;
semilogy(ns, 2.^-ns);

% Returns the fractional part of real number
function r = mod1(d)
    r = d - floor(d);
end