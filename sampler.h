#ifndef __SAMPLER__
#define __SAMPLER__

#include <boost/random/normal_distribution.hpp>
#include <boost/math/special_functions/erf.hpp>
#include <boost/math/distributions/lognormal.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>

using namespace boost::random;
using namespace boost::math;

/**
 * Variables that store the distributions of the four parameters.
 */
boost::random::mt19937 gen;
uniform_01<> u;

/** 
 * Wrapper class to sample the ODE parameters from distributions.
 */
class Sampler {
public:
    /**
     * Constructor: create all lognormal distributions.
     */
    Sampler(){
        dist_gb = createLogNormal(60., 100.);
        dist_p1 = createLogNormal(0.01, 1.0);
        dist_p2 = createLogNormal(0.0135, 1.0);
        dist_si = createLogNormal(4.17e-5, 1.e-2);
    }

    /**
     * Sample from the given distributions.
     */
    vector<double> sample( const vector<double>& x ) const {
        vector<double> result;
        
        result.push_back(60.);//quantile(*dist_gb, x[0]));
        result.push_back(quantile(*dist_p1, x[1]));
        result.push_back(quantile(*dist_p2, x[2]));
        result.push_back(quantile(*dist_si, x[3]));
        
        return result;
    }
     
private:
    /**
     * Create a lognormal distribution with given mean and variance.
     */
    lognormal* createLogNormal( double m, double v ) const {
        return new lognormal(log(m/sqrt(1. + v/(m*m))), sqrt(log(1.0 + v/(m*m))));
    }
    
    
private:
    // distributions of parameters.
    lognormal *dist_gb, *dist_p1, *dist_p2, *dist_si;
};



#endif