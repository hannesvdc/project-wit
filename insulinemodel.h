/***************************************************************************
 *
 *  Classes and functions for solving the insuline ODE model.
 *
 *  Author: Hannes Vandecasteele
 *
 **************************************************************************/
#ifndef __ODEMODEL__H__
#define __ODEMODEL__H__

#include <boost/numeric/odeint.hpp>

#include <vector>
#include <string>
#include <fstream>

using namespace std;
using namespace boost::numeric::odeint;

/**
 * Typedef for the solution of an ODE at a fixed time step.
 */
typedef std::vector<double> state_type;

/**
 * Define necessary functions before.
 */
double interpolate(const vector<double>&, const double, const double);
 
 
 
/** 
 * Class that represents the Insuline model that needs to be approximated numerically.
 */
class InsulineModel {
    
public:
    /** 
     * Constructor where all internal parameters of the model need to be set. These can be changed afterwards.
     **/
    InsulineModel(double Gb, double p1, double p2, double Si, double I): _p1(p1), _p2(p2), _Si(Si), _Ib(0.0), _Gb(Gb), _I(I) {}
    
	/**
	 * Setter p1
	 **/
	void set_p1(double p1) { _p1 = p1; }
	
	/**
	 * Setter p2
	 **/
	void set_p2(double p2) { _p2 = p2; }
	
	/**
	 * Setter Si
	 **/
	void set_Si(double Si) { _Si = Si; }
	
	/**
	 * Setter Gb
	 **/
	void set_Gb(double Gb) { _Gb = Gb; }
	
    /**
	 * Getter p1
	 **/
	double get_p1() { return _p1; }
	
	/**
	 * Getter p2
	 **/
	double get_p2() { return _p2; }
	
	/**
	 * Getter Si
	 **/
	double get_Si() { return _Si; }
	
	/**
	 * Getter Gb
	 **/
	double get_Gb() { return _Gb; }
    
    /**
     * Functor for ode integrator. This is called every timestep.
     */
    void operator()(const state_type& x, state_type& dxdt, double t) const{
        // interpolate the driving terms
        double r = 0.0; //interpolate(_R, _data_dt, t);
        double i = _I; //interpolate(_I, _data_dt, t);
        
        // and compute f(x, t) = dxdt
        dxdt[0] = -_p1*(x[0] - _Gb) - _Si*x[1]*x[0] + r;
        dxdt[1] = -_p2*x[1] + _p2*(i - _Ib);
    }

private:
    double _p1, _p2, _Si, _Ib, _Gb;
    double _I;
    vector<double> _R;
};



/**
 * Structure that contains the numerical solution of the system of ODE's
 * and is called by the time integrator to store the result.
 */
struct ODEObserver{
    /**
     * Variables that store the numerical solution.
     */
    vector<state_type>& _x;
    vector<double>& _time;
    
    /**
     * Constructor by reference.
     */
    ODEObserver(vector<state_type>& x, vector<double>& t): _x(x), _time(t) {};
    
    /**
     * Copy constructor.
     */
    ODEObserver(const ODEObserver& obs ): _x(obs._x), _time(obs._time) {};
    
     /**
     * Add a new numerical value to the solution
     */
    void operator()(const state_type& x, const double t ){
        _x.push_back(state_type(x));
        _time.push_back(t);
    }
    
    /**
     * Print the solution to the standard output stream.
     */
    void print(){
        // print the header
        cout<<"time\t";
        size_t n = _x[0].size();
        for( size_t i = 0 ; i < n ; ++i ){
            cout<<"x["<<i<<"]\t";
        }
        cout<<endl;
        
        // and the solution
        for( size_t i = 0 ; i < _x.size() ; ++i ){
            cout<<_time[i]<<"\t";
            for( size_t j = 0 ; j < n ; ++j ){
                cout<<_x[i][j]<<"\t";
            }
            cout<<endl;
        }
    }
    
    /**
     * Print solution to a file
     */
    void print( string file ){
        ofstream output(file);
        output.precision(16);
        size_t n = _x[0].size();
        
        // and the solution
        for( size_t i = 0 ; i < _x.size() ; ++i ){
            output<<_time[i]<<' ';
            for( size_t j = 0 ; j < n ; ++j ){
                output<<_x[i][j]<<' ';
            }
            output<<endl;
        }
    }
};


/**
 * Perform a linear (first order) interpolation of the given data at time t.
 * 
 * TODO: higher order interpolation
 */
double interpolate(const vector<double>& data, const double dt, const double t ){
    // interpolate linearly in [n*dt, (n+1)*dt];
    int n = (int)floor(t/dt);
    double f = (t - n*dt)/dt;
    
    return (1.0-f)*data[n] + f*data[n+1];
}


/**
 * Solve the ODE numerically and return the result in one of the parameters. The ODE is not stiff
 * hence a standard fourth order Runge-Kutta with error control is used.
 */
void solveODE(state_type x0, InsulineModel model, double dt, ODEObserver& obs) {
    // Define model and controlled stepper
    double abs_error = 1.e-12, rel_error = 1.e-12;
	typedef controlled_runge_kutta<runge_kutta_cash_karp54<state_type> > stepper;
    stepper instance(default_error_checker< double, range_algebra, default_operations >( abs_error , rel_error ));
    
    // and timestep
    integrate_const(instance, model, x0, 0.0, 800.0, dt, ODEObserver(obs._x, obs._time));
} 


#endif