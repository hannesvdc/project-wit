#ifndef __ORDERTEST__
#define __ORDERTEST__

#include <cmath>
#include <iostream>

#include "insulinemodel.h"

using namespace std;

/**
 * Determine the order of convergence of the RK model with simple parameters.
 **/
void determine_order(InsulineModel model, state_type x0, double dt, int runs, double ref_dt ){
    // First compute the numerical solution to compare with.
    vector<state_type> x_ref;
    vector<double> timeref;
    ODEObserver refsol(x_ref, timeref);
    solveODE(x0, model, ref_dt, refsol);
    
    cout<<"dt\t"<<"error"<<endl;
    // And for all the other dt's: compute and compare the numerical solutions
    for( int i = 0 ; i < runs ; ++i ){
        // setup variables
        vector<state_type> x;
        vector<double> time;
        double dti = dt / (2 << i);
        
        // compute the solution
        ODEObserver sol(x, time);
        solveODE(x0, model, dti, sol);
        //sol.print();
        
        //and determine the order of convergence by comparing the solutions at the endtimes.
        double error = norm(x_ref[x_ref.size()-1] - x[x.size()-1]);
        
        cout<<dti<<'\t'<<error<<endl;
    }
}

#endif