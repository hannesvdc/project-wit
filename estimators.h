/**
 * Functor classes to estimate all four different parameters in the insuline model.
 */
#ifndef __ESTIMATORS__
#define __ESTIMATORS__

#include <vector>

using namespace std;

class QMCFunction{
public:
    QMCFunction(string s): name(s) {}
    
    virtual double operator()(const vector<double>& ) { return 0; }
    
    string getName(){ return name; }
    
protected:
    string name;
};

class GbEstimator: public QMCFunction {
public:
    GbEstimator(): QMCFunction("Gb") {};
    
    virtual double operator()(const vector<double>& params ) {
        return params[0];
    }
};

class P1Estimator: public QMCFunction {
public:
    P1Estimator(): QMCFunction("P1") {}
    
    virtual double operator()(const vector<double>& params ) {
        return params[1];
    }
};

class P2Estimator: public QMCFunction {
public:
    P2Estimator(): QMCFunction("P2") {}
    
    virtual double operator()(const vector<double>& params ) {
        return params[2];
    }
};

class SiEstimator: public QMCFunction {
public:
    SiEstimator(): QMCFunction("Si") {}
    
    virtual double operator()(const vector<double>& params ) {
        return params[3];
    }
};

#endif