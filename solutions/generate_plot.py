import matplotlib.pyplot as plt
import numpy as np

# Gather the data
datafile = open("qmcconvergence.txt", "r")
array = np.array([[float(x) for x in line.split()] for line in datafile])
n = array[:, 0];
var = array[:, 2];
s = n.size

# and make the convergence plot
plt.gca().set_yscale('log')
plt.plot(n, var);

a = np.empty(s)
a.fill(2.0)
print a
print n
y = 1.0/np.sqrt(np.power(a, n))
z = 1.0/np.power(a, n)

plt.plot(n, y)
plt.plot(n, z)
plt.xlabel("2^n QMC points")
plt.ylabel("Error")
plt.legend(["Numerical Error", "O(N^-0.5)", "O(N^-1)" ])
plt.title("Global error of Quasi Monte Carlo \n f(x) = (1+B_2(x))")
plt.show()
