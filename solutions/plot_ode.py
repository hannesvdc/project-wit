# Plot solutions ODE

# Import packages
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

# Import data from first command line argument
arglist = sys.argv
filename_ref = str(arglist[1])

tref = []
Gref = []
Xref = []

with open(filename_ref + '.out','r') as file:
    for row in file:
		a, b, c = row.split()
		tref.append(a)
		Gref.append(b)
		Xref.append(c)

# If second command line argument apparent, import data
if len(sys.argv) == 3:
	filename_sim = str(arglist[2])
	tsim = []
	Gsim = []
	Xsim = []
	
	with open(filename_sim + '.out','r') as file:
		for row in file:
			a, b, c = row.split()
			tsim.append(a)
			Gsim.append(b)
			Xsim.append(c)


# Plot G(t)
plt.figure(1)
plt.subplot(2,1,1)
plt.plot(tref,Gref, 'r-', linewidth=2, label='Gref')
if len(sys.argv) == 3:
	plt.plot(tsim,Gsim, 'k-.', linewidth=2,label='Gsim')
plt.xlabel('Time')
plt.ylabel('G(t)')
plt.legend()
plt.title('Glucose level in time')

# Plot X(t)
plt.subplot(2,1,2)
plt.plot(tref,Xref, 'b-', linewidth=2, label='Xref')
if len(sys.argv) == 3:
	plt.plot(tsim,Xsim, 'k-.', linewidth=2,label='Xsim')
plt.xlabel('Time')
plt.ylabel('X(t)')
plt.title('Remote insulin in time')
plt.legend()
plt.show()