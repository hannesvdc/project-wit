# Plot log2(stdev) of the QMC-approximation in function of the number of samples
# Import packages
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

# Import data
arglist = sys.argv
filename = str(arglist[1])


order = []
stdev = []


with open(filename + '.out','r') as file:
    for row in file:
		a, b = row.split()
		order.append(float(a))
		stdev.append(float(b))

order = np.array(order)
stdev = np.array(stdev)

# Make the plot
plt.figure(1)
order += 4.
plt.plot(order,np.log2(stdev), 'r-o', linewidth=2)
plt.plot(order,np.log2(1./2.**order)-10,'b-o', linewidth=2)
plt.xlabel('log2(Number of function evaluations)')
plt.ylabel('log2(stdev(estimate))')
plt.title('Order plot standard deviation')
plt.show()