# Import packages
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

# Read data, command line 
arglist = sys.argv
filename = str(arglist[1])

est = []
abscis = []
density = []

with open(filename + '.out','r') as file:
    lines = file.readlines()
    
    # read the estimates of the bayesian estimator
    aantal = int(lines[0])
    for i in range(aantal):
        est.append(float(lines[i+1]))
    
    
    # read the posterior distribution
    for i in range(aantal+1, len(lines)):
        row = lines[i]
        a, b = row.split()
        abscis.append(float(a))
        density.append(float(b))


abscis = np.asarray(abscis)
density = np.asarray(density)


# Ask for the real value
real_value = float(raw_input("Please enter the real value of the variable: "))

# Plot posterior
width = abscis[1]-abscis[0]

fig, ax = plt.subplots()
rects1 = ax.bar(abscis, density, width, color='r', label='Posterior')

ax.set_title('Posterior and prior distribution of variable')

# Plot prior
# Ask for the prior parameters
m = float(raw_input("Please enter the mean of the prior distribution: "))
v = float(raw_input("Please enter the variance of the prior distribution: "))

mu, sigma = np.log(m/np.sqrt(1. + v/(m*m))), np.sqrt(np.log(1.0 + v/(m*m)))
s = np.random.lognormal(mu, sigma, 1000)

x = np.linspace(abscis[0], abscis[-1], 10000)
pdf = (np.exp(-(np.log(x) - mu)**2 / (2 * sigma**2))/(x * sigma * np.sqrt(2 * np.pi)))

plt.plot(x, pdf, linewidth=2, color='b', label='Prior')
plt.plot([est[0]],[max(density)*0.9],'yo', ms=10., label='Estimated variable')
plt.plot([real_value],[max(density)*0.9],'bo', ms=10., label='Real value')
plt.axis('tight')
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
plt.show()