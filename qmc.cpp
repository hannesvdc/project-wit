/*******************************************************************
 *
 * Test file for QMC integration using 
 * simple functions.
 *
 * Author: Hannes Vandecasteele
 *
 ******************************************************************/
#ifndef __QMC__
#define __QMC__

#include <time.h>
#include <cmath>
#include <fstream>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>

#include "insulinemodel.h"
#include "vectormath.h"

using namespace boost::random;

/**
 * modulo 1
 */
double mod1(double x){
    return x - floor(x);
}

/**
 * Retreive a generating vector from file.
 */
vector<double> read_vector(string f){
    ifstream infile(f);
    vector<double> v;
    string input;
    getline(infile, input);
    
    while( input != "" ){
        v.push_back(atoi(input.c_str()));
        getline(infile, input);
    }
    
    return v;
}

/** 
 * Basic quasi Monte Carlo integrator for the insuline problem.
 */
template<typename Function>
vector<double> qmc(Function f, vector<double> z, int genpow, int npow){
    // set shift generator
    boost::random::mt19937 gen;
    uniform_01<> u;
    
    //get the generator vector and the number of points
    int s = z.size();
    int n = (1 << npow);
    
    // and sample the function such that we approximate the integral
    vector<double> integrals;
    for( int i = 0 ; i < 8 ; ++i ){
        // generate the shifts
        vector<double> shift(s, 0.0);
        for( int j = 0 ; j < s ; ++j ) shift[j] = u(gen);
        
        // store the integral value
        double integral = 0.0;
        
        // iterate over the function values
        for( int k = 0 ; k < n ; ++k ){
            // calculate the lattice points and apply the shifts
            vector<double> x = mod1( ((double)k*z)/n + shift);

            // and compute the function value
            integral += f(x);
        }
        integral /= n;
        integrals.push_back(integral);
    }
    
    return integrals;
}

/** 
 * Sample function: Bernouilli polynomials.
 */
class Bernouilli {
public:
    double operator()(const vector<double>& x){
        return (1.0 + x[0]*x[0] - x[0] + 1./6.) * (1.0 + x[1]*x[1] - x[1] + 1./6.);
    }
};

int main(){
    ofstream outfile("qmcconvergence.txt");
    vector<double> z = read_vector("exod2_base2_m20_CKN.txt");
    for( int npow = 5 ; npow <= 18 ; ++npow ){
        vector<double> integrals = qmc(Bernouilli(), z, 20, npow);
    
        double mean = 0.0;
        for( double d: integrals ){
            mean += d;
        }
        mean = mean/integrals.size();
        
        double var = 0.0;
        for( double d: integrals ){
            double diff = d - mean;
            var += diff*diff;
        }
        var = sqrt(var);

        outfile<<npow<<" "<<mean<<" "<<var<<endl;
    }
    
    
    return 0;
}

#endif